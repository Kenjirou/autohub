'use strict';

angular.module('autohub')
.controller('HubController', ['$scope', 'hubFactory', 'baseURL', function($scope, hubFactory, baseURL) {
  $scope.baseURL = baseURL;
  $scope.showGallery = false;
  $scope.message = 'Loading ...';

  $scope.brands = [
    'Alfa Romeo',
    'Audi',
    'BMW',
    'Chevrolet',
    'Citroen',
    'Fiat',
    'Ford',
    'Honda',
    'Hyundai',
    'Infiniti',
    'Kia',
    'Land Rover',
    'Lexus',
    'Mazda',
    'Mercedes-Benz',
    'Mitsubishi',
    'Nissan',
    'Opel',
    'Peugeot',
    'Porsche',
    'Renault',
    'SEAT',
    'Skoda',
    'Subaru',
    'Suzuki',
    'Toyota',
    'Volkswagen',
    'Volvo'
  ];

  $scope.cars = hubFactory.getCars().query(
    function(response) {
      $scope.cars = response;
      $scope.showGallery = true;
    },
    function(response) {
      $scope.message = 'Error: ' + response.status + ' ' + response.statusText;
    }
  );

}])
;

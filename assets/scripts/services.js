'use strict';

angular.module('autohub')
.constant('baseURL', 'http://localhost:3000/')
.service('hubFactory', ['$resource', 'baseURL', function ($resource, baseURL) {
  this.getCars = function() {
    return $resource(baseURL + 'cars', null, {
      'update': {
        method: 'PUT'
      }
    });
  };
}])
;

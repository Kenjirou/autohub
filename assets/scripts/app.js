'use strict';

angular.module('autohub', ['ui.router', 'ngResource'])
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('app', {
    url: '/',
    views: {
      'header': {
        templateUrl: 'templates/header.html'
      },
      'content': {
        templateUrl: 'templates/home.html',
        controller: 'HubController'
      },
      'footer': {
        templateUrl: 'templates/footer.html'
      }
    }
  })
  ;

  $urlRouterProvider.otherwise('/');
});
